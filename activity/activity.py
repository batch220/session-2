  
Year = int(input("Enter the year: "))  

if((Year % 400 == 0) or  
     (Year % 100 != 0) and  
     (Year % 4 == 0)):   
    print("Given Year is a leap Year")   
else:  
    print ("Given Year is not a leap Year")  


Row_num = int(input("Enter number of row: ")) 
Column_num= int(input("Enter number of column: ")) 


for x in range(Row_num):
    for y in range(Column_num):
        print('*',end = ' ')
    print()